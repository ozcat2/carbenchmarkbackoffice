const { injectBabelPlugin } = require('react-app-rewired')
const rewireLess = require('react-app-rewire-less-with-modules')
const rewireMobX = require('react-app-rewire-mobx')
// const rewireAliases = require('react-app-rewire-aliases')
const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}
module.exports = function override (config, env) {
  // config = rewireAliases.aliasesOptions({
  // themes: path.resolve(__dirname, `${paths.appSrc}/themes`)
  // })(config, env);
  config.resolve = {
    alias: {
      themes: path.resolve(__dirname, 'src/themes'),
      pages: path.resolve(__dirname, 'src/pages'),
      routes: path.resolve(__dirname, 'src/routes'),
      '@': resolve('src'),
    }
  }
  config = injectBabelPlugin(['import', { libraryName: 'antd', style: true }], config)
  config = rewireLess(config, env, {
    javascriptEnabled: true
  })
  config = rewireMobX(config, env)
  return config
}
