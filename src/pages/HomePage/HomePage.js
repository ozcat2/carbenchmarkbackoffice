import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { inject, observer } from 'mobx-react'
import { Row, Col, Layout } from 'antd'
import style from './HomePage.module.less'
@inject('userStore')
@observer
export default class HomePage extends Component {
  static propTypes = {
    userStore: PropTypes.object
  }

  render () {
    return (
      <div>
        <div className={style['test-module']}>
          hello
        </div>
        <Layout style={{
          maxWidth: 1170
        }}>
          <Row>
            <Col span={12}>
              <div style={{
                width: '100%',
              }}>
                <Row type='flex' className={style['text-center']}>
                  <Col span={4}>col4</Col>
                  <Col span={4}>col4</Col>
                  <Col span={4}>col4</Col>
                  <Col span={4}>col4</Col>
                  <Col span={4}>col4</Col>
                </Row>
              </div>
            </Col>
            <Col span={12}>
              <div style={{
                width: '100%'
              }}>col2</div>
            </Col>
          </Row>
        </Layout>
      </div>
    )
  }
}
