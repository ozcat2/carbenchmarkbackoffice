import React, { Component } from 'react'
import { Button, Table, Icon, Divider, Tabs } from 'antd'
import Evaluation from '@/components/Evaluation/Evaluation'
import Criteria from '@/components/Evaluation/Criteria'
import Score from '@/components/Evaluation/Score'
import Car from '@/components/Evaluation/Car'
import PropTypes from 'prop-types'
import s from './EvaluationPage.module.less'
const TabPane = Tabs.TabPane
export default class EventPage extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render () {
    return (
      <div>
        <div>
          <h2 className={s.title}>Evaluation Event</h2>
        </div>
        <div>
          <Tabs type='card'>
            <TabPane tab='Evaluation' key='1'>
              <Evaluation />
            </TabPane>
            <TabPane tab='Evaluation Criteria' key='2'>
              <Criteria />
            </TabPane>
            <TabPane tab='Evaluation Score' key='3'>
              <Score />
            </TabPane>
            <TabPane tab='Evaluation Car' key='4'>
              <Car />
            </TabPane>
          </Tabs>
        </div>
      </div>
    )
  }
}
