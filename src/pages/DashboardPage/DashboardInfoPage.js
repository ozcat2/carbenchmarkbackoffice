import React, { Component } from 'react'
import { Button, Table, Icon, Divider, Tabs } from 'antd'
import Summary from '@/components/Dashboard/Summary'
import VehicleReport from '@/components/Dashboard/VehicleReport'
import MarkerChart from '@/components/Dashboard/MarkerChart'
import CandleSticksChart from '@/components/Dashboard/CandleSticksChart'
import RadarChart from '@/components/Dashboard/RadarChart'
import PropTypes from 'prop-types'
import s from './DashboardInfoPage.module.less'
const TabPane = Tabs.TabPane
export default class DashboardInfoPage extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render () {
    return (
      <div>
        <div>
          <h2 className={s.title}>Evaluation Event</h2>
        </div>
        <div>
          <Tabs type='card'>
            <TabPane tab='Summary Assessment' key='1'>
              <Summary />
            </TabPane>
            <TabPane tab='Vehicle Report' key='2'>
              <VehicleReport />
            </TabPane>
            <TabPane tab='Marker Chart' key='3'>
              <MarkerChart />
            </TabPane>
            <TabPane tab='Candle Sticks Car' key='4'>
              <CandleSticksChart />
            </TabPane>
            <TabPane tab='Radar Chart' key='5'>
              <RadarChart />
            </TabPane>
          </Tabs>
        </div>
      </div>
    )
  }
}
