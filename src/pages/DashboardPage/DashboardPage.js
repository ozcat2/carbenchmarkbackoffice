import React, { Component } from 'react'
import { Button, Table, Icon, Divider } from 'antd'
import SearchFilter from '@/components/SearchFilter/SearchFilter'
import PropTypes from 'prop-types'
import s from './DashboardPage.module.less'
const columns = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name',
  render: text => <a href='javascript:;'>{text}</a>,
}, {
  title: 'Age',
  dataIndex: 'age',
  key: 'age',
}, {
  title: 'Address',
  dataIndex: 'address',
  key: 'address',
}, {
  title: '',
  key: 'action',
  render: (text, record) => (
    <div>
      <Button type='primary' style={{ marginRight: 10 }}>Edit</Button>
      <Button type='danger'>Delete</Button>
    </div>
  ),
}]

const data = [{
  key: '1',
  name: 'John Brown',
  age: 32,
  address: 'New York No. 1 Lake Park',
}, {
  key: '2',
  name: 'Jim Green',
  age: 42,
  address: 'London No. 1 Lake Park',
}, {
  key: '3',
  name: 'Joe Black',
  age: 32,
  address: 'Sidney No. 1 Lake Park',
}]
export default class DashboardPage extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render () {
    return (
      <div>
        <div>
          <h2 className={s.title}>Evaluation Event</h2>
        </div>
        <div className='box-wrapper'>
          <Button type='primary' className='default-button'>Generate New Event</Button>
        </div>
        <div className='box-wrapper'>
          <SearchFilter />
        </div>
        <div className='box-wrapper'>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
        </div>
      </div>
    )
  }
}
