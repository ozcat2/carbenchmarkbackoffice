import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Table, Input, Form } from 'antd'
import s from './EventInfoPage.module.less'
import classNames from 'classnames'
import BaseModal from '@/components/Modal/BaseModal'
import { Link } from 'react-router-dom'
const FormItem = Form.Item
const columns = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name',
  render: text => <a href='javascript:;'>{text}</a>,
}, {
  title: 'Age',
  dataIndex: 'age',
  key: 'age',
}, {
  title: 'Address',
  dataIndex: 'address',
  key: 'address',
}, {
  title: '',
  key: 'action',
  render: (text, record) => (
    <div>
      <Button type='primary' style={{ marginRight: 10 }}>See Progress</Button>
    </div>
  ),
}]

const data = [{
  key: '1',
  name: 'John Brown',
  age: 32,
  address: 'New York No. 1 Lake Park',
}, {
  key: '2',
  name: 'Jim Green',
  age: 42,
  address: 'London No. 1 Lake Park',
}, {
  key: '3',
  name: 'Joe Black',
  age: 32,
  address: 'Sidney No. 1 Lake Park',
}]
export default class EventInfoPage extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render () {
    return (
      <div>
        <div className={s.titleWrapper}>
          <h2 className={s.title}>E0001: Rayong Test</h2>
          <h2 className={classNames(s.title, s.blue)}>Event Status 75 %</h2>
        </div>
        <div className='box-wrapper'>
          description
        </div>
        <div className='box-wrapper center'>Evaluator</div>
        <div className='box-wrapper'>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
        </div>
        <div className='box-wrapper center'>Evaluation</div>
        <div className='box-wrapper'>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
        </div>
        <BaseModal
          title='Evaluation Progress'
          visible={false} >
          <div className={s.titleWrapper}>
            <h2 className={s.title}>A0001: Kittikhun Khomwaingsa</h2>
            <h2 className={classNames(s.title, s.blue)}>Status 75%</h2>
          </div>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
        </BaseModal>
        <BaseModal
          title='Generate Evaluation Event'
        >
          <div className='box-wrapper'>
            <Form layout='vertical'>
              <FormItem
                label='Field A'
              >
                <Input placeholder='input placeholder' />
              </FormItem>
              <FormItem
                label='Field A'
              >
                <Input placeholder='input placeholder' />
              </FormItem>
              <FormItem
                label='Field A'
              >
                <Input placeholder='input placeholder' />
              </FormItem>
            </Form>
          </div>
          <div className='box-wrapper center'>Evaluator</div>
          <div className='box-wrapper'>
            <Button type='primary' className='default-button'>Generate New Evaluator</Button>
          </div>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
          <div className='box-wrapper center'>Evaluation</div>
          <div className='box-wrapper'>
            <Button type='primary' className='default-button'>Generate New Evaluation</Button>
          </div>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
        </BaseModal>
        <BaseModal
          title='Generate New Evaluator'
        >
          <Form layout='vertical'>
            <FormItem
              label='Field A'
            >
              <Input placeholder='input placeholder' />
            </FormItem>
            <FormItem
              label='Field A'
            >
              <Input placeholder='input placeholder' />
            </FormItem>
            <FormItem
              label='Field A'
            >
              <Input placeholder='input placeholder' />
            </FormItem>
          </Form>
          <Button type='primary'>Generate</Button>
        </BaseModal>
      </div>
    )
  }
}
