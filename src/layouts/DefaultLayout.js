import React from 'react'
import { Layout, Icon, Menu } from 'antd'
import s from './DefaultLayout.module.less'
const { Header, Content, } = Layout
const { SubMenu } = Menu
export default class DefaultLayout extends React.Component {
  render () {
    return (
      <Layout className={s.layout}>
        <Header className={s.header}>
          <div className={s.button}>
            <Icon type='bars' />
          </div>
          <div className={s.rightWarpper}>
            <Menu mode='horizontal'>
              <SubMenu
                style={{
                  float: 'right'
                }}
                title={<span>
                  <Icon type='user' />
                Username
                </span>}
              >
                <Menu.Item key='logout'>
              Sign out
                </Menu.Item>
              </SubMenu>
            </Menu>
          </div>
        </Header>
        <Content className={s.content}>
          {this.props.children}
        </Content>
      </Layout>
    )
  }
}
