import React, { Component } from 'react'
import { Route, Switch, BrowserRouter } from 'react-router-dom'
import HomePage from '@/pages/HomePage/HomePage'
import EventPage from '@/pages/EventPage/EventPage'
import EventInfoPage from '@/pages/EventPage/EventInfoPage'
import EvaluationPage from '@/pages/EvaluationPage/EvaluationPage'
import DashboardPage from '@/pages/DashboardPage/DashboardPage'
import DashboardInfoPage from '@/pages/DashboardPage/DashboardInfoPage'
import LogInPage from '@/pages/LogInPage/LogInPage'
import DefaultLayout from '@/layouts/DefaultLayout'
const HomePageLayout = () => (
  <DefaultLayout>
    <HomePage />
  </DefaultLayout>
)
const EventPageLayout = () => (
  <DefaultLayout>
    <EventPage />
  </DefaultLayout>
)
const EventInfoPageLayout = () => (
  <DefaultLayout>
    <EventInfoPage />
  </DefaultLayout>
)
const EvaluationPageLayout = () => (
  <DefaultLayout>
    <EvaluationPage />
  </DefaultLayout>
)
const DashboardPageLayout = () => (
  <DefaultLayout>
    <DashboardPage />
  </DefaultLayout>
)
const DashboardInfoPageLayout = () => (
  <DefaultLayout>
    <DashboardInfoPage />
  </DefaultLayout>
)
export default class MainRoute extends Component {
  render () {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={HomePageLayout} />
          <Route exact path='/event' component={EventPageLayout} />
          <Route exact path='/event/info' component={EventInfoPageLayout} />
          <Route exact path='/evaluation' component={EvaluationPageLayout} />
          <Route exact path='/dashboard' component={DashboardPageLayout} />
          <Route exact path='/dashboard/info' component={DashboardInfoPageLayout} />
          <Route exact path='/login' component={LogInPage} />
        </Switch>
      </BrowserRouter>
    )
  }
}
