import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal } from 'antd'
import s from './BaseModal.module.less'
export default class BaseModal extends Component {
  static propTypes = {
    title: PropTypes.string,
  }

  render () {
    return (
      <Modal
        {...this.props}
        footer={null}
        className={s.modal}
      >
        {this.props.children}
      </Modal>
    )
  }
}
