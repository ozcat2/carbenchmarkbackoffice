import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table } from 'antd'
export default class Summary extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render () {
    return (
      <div>
        <div className='ant-table-wrapper'>
          <div className='ant-spin-nested-loading'>
            <div className='ant-spin-container'>
              <div className='ant-table ant-table-default ant-table-scroll-position-left'>
                <div className='ant-table-content'>
                  <div className='ant-table-body'>
                    <table className=''>
                      <colgroup>
                        <col />
                        <col />
                        <col />
                        <col />
                      </colgroup>
                      <thead className='ant-table-thead'>
                        <tr>
                          <th className=''><span>Name</span></th>
                          <th className=''><span>Age</span></th>
                          <th className=''><span>Address</span></th>
                          <th className=''><span /></th>
                        </tr>
                      </thead>
                      <tbody className='ant-table-tbody'>
                        <tr className='ant-table-row ant-table-row-level-0' data-row-key='1'>
                          <td className=''><a href='javascript:;'>John Brown</a></td>
                          <td className=''>32</td>
                          <td className=''>New York No. 1 Lake Park</td>
                          <td className=''>
                            <div><button type='button' className='ant-btn ant-btn-primary'><span>Edit</span></button><button type='button' className='ant-btn ant-btn-danger'><span>Delete</span></button></div>
                          </td>
                        </tr>
                        <tr className='ant-table-row ant-table-row-level-0' data-row-key='2'>
                          <td className=''><a href='javascript:;'>John Brown</a></td>
                          <td className=''>42</td>
                          <td className=''>London No. 1 Lake Park</td>
                          <td className=''>
                            <div><button type='button' className='ant-btn ant-btn-primary'><span>Edit</span></button><button type='button' className='ant-btn ant-btn-danger'><span>Delete</span></button></div>
                          </td>
                        </tr>
                        <tr className='ant-table-row ant-table-row-level-0' data-row-key='3'>
                          <td className=''><a href='javascript:;'>John Brown</a></td>
                          <td className=''>32</td>
                          <td className=''>Sidney No. 1 Lake Park</td>
                          <td className=''>
                            <div><button type='button' className='ant-btn ant-btn-primary'><span>Edit</span></button><button type='button' className='ant-btn ant-btn-danger'><span>Delete</span></button></div>
                          </td>
                        </tr>
                      </tbody>
                      <thead className='ant-table-thead'>
                        <tr>
                          <th className=''><span>Name</span></th>
                          <th className=''><span>Age</span></th>
                          <th className=''><span>Address</span></th>
                          <th className=''><span /></th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
