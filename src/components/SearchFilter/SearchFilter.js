import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Input, Select } from 'antd'
import s from './SearchFilter.module.less'
const Search = Input.Search
const InputGroup = Input.Group
const Option = Select.Option
export default class SearchFilter extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render () {
    return (
      <div>
        <InputGroup compact style={{ display: 'flex' }}>
          <Select defaultValue='Home' style={{ flex: 0 }}>
            <Option value='Home'>Home</Option>
            <Option value='Company'>Company</Option>
          </Select>
          <Search
            className={s['search-input']}
            style={{ flex: 1 }}
            placeholder='input search text'
            onSearch={value => console.log(value)}
            enterButton
          />
        </InputGroup>
      </div>
    )
  }
}
