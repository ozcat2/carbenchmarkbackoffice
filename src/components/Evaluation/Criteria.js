import React, { Component } from 'react'
import PropTypes from 'prop-types'
import s from './Criteria.module.less'
import { Button, Table, Icon, Divider, Form, Input } from 'antd'
import SearchFilter from '@/components/SearchFilter/SearchFilter'
import classNames from 'classnames'
import BaseModal from '@/components/Modal/BaseModal'
const FormItem = Form.Item
const columns = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name',
  render: text => <a href='javascript:;'>{text}</a>,
}, {
  title: 'Age',
  dataIndex: 'age',
  key: 'age',
}, {
  title: 'Address',
  dataIndex: 'address',
  key: 'address',
}, {
  title: '',
  key: 'action',
  render: (text, record) => (
    <div>
      <Button type='primary' style={{ marginRight: 10 }}>Edit</Button>
      <Button type='danger'>Delete</Button>
    </div>
  ),
}]

const data = [{
  key: '1',
  name: 'John Brown',
  age: 32,
  address: 'New York No. 1 Lake Park',
}, {
  key: '2',
  name: 'Jim Green',
  age: 42,
  address: 'London No. 1 Lake Park',
}, {
  key: '3',
  name: 'Joe Black',
  age: 32,
  address: 'Sidney No. 1 Lake Park',
}]
export default class Criteria extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render () {
    return (
      <div>
        <div className={classNames(s.inlineWrapper, 'box-wrapper')}>
          <Button type='primary' className='default-button'>Generate New Event</Button>
          <SearchFilter />
        </div>
        <div className='box-wrapper'>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
        </div>
        <BaseModal
          title='Generate Evaluation'
        >
          <div className='box-wrapper'>
            <Form layout='vertical'>
              <FormItem
                label='Field A'
              >
                <Input placeholder='input placeholder' />
              </FormItem>
              <FormItem
                label='Field A'
              >
                <Input placeholder='input placeholder' />
              </FormItem>
              <FormItem
                label='Field A'
              >
                <Input placeholder='input placeholder' />
              </FormItem>
            </Form>
          </div>
          <div className='box-wrapper center'>Evaluator</div>
          <div className='box-wrapper'>
            <Button type='primary' className='default-button'>Generate New Evaluator</Button>
          </div>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
          <div className='box-wrapper center'>Evaluation</div>
          <div className='box-wrapper'>
            <Button type='primary' className='default-button'>Generate New Evaluation</Button>
          </div>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={(record) => console.log('record', record)} />
        </BaseModal>
      </div>
    )
  }
}
