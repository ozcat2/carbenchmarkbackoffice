import React, { Component } from 'react'
import { Provider } from 'mobx-react'
import MainRoute from '@/routes'
import { stores } from '@/stores'
import './App.less'
class App extends Component {
  render () {
    return (
      <Provider {...stores}>
        <MainRoute />
      </Provider>
    )
  }
}

export default App
