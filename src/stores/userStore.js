import { observable, action } from 'mobx'

class UserStore {
  @observable currentUser = 'best'
}

export default new UserStore()
